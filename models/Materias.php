<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materias".
 *
 * @property int $id
 * @property int $codigo
 * @property string $nombre
 * @property string $materias
 */
class Materias extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'materias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'nombre'], 'required'],
            [['codigo'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'codigo', 'nombre'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'codigo' => 'Codigo',
            'nombre' => 'Nombre'
        ];
    }
}
