<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\mongodb\Query;
use yii\helpers\ArrayHelper;

$query = new Query();
$query->from('materias');

/* @var $this yii\web\View */
/* @var $model app\models\Estudiantes */
/* @var $form yii\widgets\ActiveForm */
//$materias =  ["1" => "Español", "2" => "Matematicas"];
$materias= ArrayHelper::map($query->all(),'nombre','nombre');
//print_r($materias);
?>

<div class="estudiantes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'materias')->widget(Select2::classname(), [
        'data' => $materias,
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccione la materia ...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
